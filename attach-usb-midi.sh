#!/bin/sh
 
DEV_NAME=$1
TARGET_DEV_NO=$(aconnect -il | sed -n "s/^client\ \([0-9]\+\):.*'$DEV_NAME'.*$/\1/ p")

SOURCE_DEV_NO=$(aconnect -il | sed -n "s/^client\ \([0-9]\+\):.*'Juce Midi Output'.*$/\1/ p")

if [ ! $2 ]; then
  echo please specify which port to sue
  exit 1
elif [ $2 = 'A' ]; then
  SOURCE_PORT_NO=3
elif [ $2 = 'B' ]; then
  SOURCE_PORT_NO=4
elif [ $2 = 'C' ]; then
  SOURCE_PORT_NO=5
else
  echo bad port $2 given
  exit 1
fi

while [ ! $SOURCE_DEV_NO ]; do
  sleep 1
  echo waiting for MPC software to start...
  SOURCE_DEV_NO=$(aconnect -il | sed -n "s/^client\ \([0-9]\+\):.*'Juce Midi Output'.*$/\1/ p")
done

if [ $TARGET_DEV_NO ]; then
  echo connecting $SOURCE_DEV_NO:3 $TARGET_DEV_NO:0
  aconnect $SOURCE_DEV_NO:$SOURCE_PORT_NO $TARGET_DEV_NO:0
fi
