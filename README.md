# USB MIDI out for MPC Live/X/Force

These files allow MIDI output over external USB devices.

`attach-usb-midi.sh` goes in /bin

The service files go in /usr/lib/systemd/system/

For the moment, this requires shell access. Either open the device and use the console port, or modify the firmware image to enable SSH. I am not comfortable providing remote assistance with the latter, since it carries some risk of bricking your device.

Once everything is in place, the service should then be enabled from the command line by first unmounting the overlay fs on /etc, remounting root to be rewritable, and then enabling the service itself:

    $ umount /etc
    $ mount / -o rw,remount
    $ systemctl enable attach-usb-midi-a@DEVICE_NAME
    $ reboot

Where DEVICE_NAME is the name of the device as reported by the `aconnect -ol` command.

This mirrors anything sent out on MIDI port A to the USB device. A secondary service allows to capture what goes to port B or C (remote) instead.

The MPC app does not appear to initialize the unused (D, E) ports that are present in the UI for compatibility with the MPC X, so it is not possible to have more than two devices, and we have to occupy the same slots as the DIN ports. MIDI channels still exist, though!

Note that this requires the device to be attached on bootup. There's a way around that with udev rules, I'll probably make those soon.
